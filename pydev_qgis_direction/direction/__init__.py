# -*- coding: utf-8 -*-
"""
/***************************************************************************
 Direction
                                 A QGIS plugin
 description of direction plugin
                             -------------------
        begin                : 2014-09-30
        copyright            : (C) 2014 by Thomas Schuster
        email                : thomas.schus@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""

def classFactory(iface):
    # load Direction class from file Direction
    from direction import Direction
    return Direction(iface)
