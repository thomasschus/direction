# -*- coding: utf-8 -*-
"""
/***************************************************************************
 Direction
                                 A QGIS plugin
 description of direction plugin
                              -------------------
        begin                : 2014-09-30
        copyright            : (C) 2014 by Thomas Schuster
        email                : thomas.schus@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
# Import the PyQt and QGIS libraries
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *
from qgis.gui import QgsMessageBar
# Initialize Qt resources from file resources.py
import resources_rc
# Import the code for the dialog
from directiondialog import DirectionDialog
import os.path
import requests
import json

class Direction:
    ''' class docs'''
    def __init__(self, iface):
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value("locale/userLocale")[0:2]
        localePath = os.path.join(self.plugin_dir, 'i18n',
                                  'direction_{}.qm'.format(locale))

        if os.path.exists(localePath):
            self.translator = QTranslator()
            self.translator.load(localePath)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

        # Create the dialog (after translation) and keep reference
        self.dlg = DirectionDialog()

    def initGui(self):
        # Create action that will start plugin configuration
        self.action = QAction(
            QIcon(":/plugins/direction/icon.png"),
            u"direction plugin", self.iface.mainWindow())
        # connect the action to the run method
        self.action.triggered.connect(self.run)

        # Add toolbar button and menu item
        self.iface.addToolBarIcon(self.action)
        self.iface.addPluginToMenu(u"&direction", self.action)

    def unload(self):
        # Remove the plugin menu item and icon
        self.iface.removePluginMenu(u"&direction", self.action)
        self.iface.removeToolBarIcon(self.action)

    # run method that performs all the real work
    def run(self):
        # show the dialog
        self.dlg.show()
        # Run the dialog event loop
        result = self.dlg.exec_()
        # See if OK was pressed
        if result == 1:
            # send request to google direction service
            response = self._sendRequest()

            # convert response to json
            jsonResponse = response.json()

            # catch status of response
            #
            #  OK: indicates the response contains a valid result.
            #  NOT_FOUND: indicates at least one of the locations
            #             specified in the request's origin,
            #             destination, or waypoints could not be geocoded.
            #  ZERO_RESULTS: indicates no route could be found between
            #             the origin and destination.
            #  MAX_WAYPOINTS_EXCEEDED: indicates that too many waypoints
            #             were provided in the request The maximum allowed
            #             waypoints is 8, plus the origin, and destination.
            #             (Google Maps API for Work customers may contain
            #             requests with up to 23 waypoints.)
            #  INVALID_REQUEST: indicates that the provided request was
            #             invalid. Common causes of this status include
            #             an invalid parameter or parameter value.
            #  OVER_QUERY_LIMIT: indicates the service has received too many
            #             requests from your application within the allowed
            #             time period.
            #  REQUEST_DENIED: indicates that the service denied use of the
            #             directions service by your application.
            #  UNKNOWN_ERROR: indicates a directions request could not be
            #             processed due to a server error. The request may
            #             succeed if you try again.
            
            # response status
            status = jsonResponse['status']

            self._process(jsonResponse)

            #with open(r'/home/tschuster/Desktop/sample.json', 'w') as myfile:
            #    json.dump(response.json(), myfile)

            self.iface.messageBar().pushMessage("Info", "Google Directions Response Status: %s" % status, level=QgsMessageBar.INFO)

    def _sendRequest(self):
        try:
            # parameters dictionary
            parameter = {}

            # mandatory parameters
            origin = self.dlg.lineEdit_origin.text()
            parameter['origin'] = origin
            QgsMessageLog.logMessage('origin=%s' % origin, 'Directions', QgsMessageLog.INFO)

            destination = self.dlg.lineEdit_destination.text()
            parameter['destination'] = destination
            QgsMessageLog.logMessage('destination=%s' % destination, 'Directions', QgsMessageLog.INFO)

            # optional parameters
            alternatives = str(self.dlg.checkBox_alternatives.isChecked()).lower()
            parameter['alternatives'] = alternatives
            QgsMessageLog.logMessage('alternatives=%s' % alternatives, 'Directions', QgsMessageLog.INFO)

            # routing restrictions
            restrictions = []
            if self.dlg.checkBox_avoidTolls.isChecked():
                restrictions.append('tolls')
            if self.dlg.checkBox_avoidHighways.isChecked():
                restrictions.append('highways')
            if self.dlg.checkBox_avoidFerries.isChecked():
                restrictions.append('ferries')
            parameter['avoid'] = '|'.join(restrictions)
            QgsMessageLog.logMessage('avoid=%s' % '|'.join(restrictions), 'Direction', QgsMessageLog.INFO)

            # base URL
            url = "http://maps.googleapis.com/maps/api/directions/json"

            return requests.get(url, params=parameter)
        except:
            self.iface.messageBar().pushMessage("Error", 'Sending request failed', level=QgsMessageBar.CRITICAL)

    def _process(self, jsonResponse):
        try: 
            # create layer for overview line
            vl = QgsVectorLayer('LineString?crs=epsg:4326', 'overview', 'memory')
            pr = vl.dataProvider()

            # enter editing mode
            vl.startEditing()

            # add fields
            #pr.addAttributes([QgsField('name', QVariant.String)])

            # add feature
            feat = QgsFeature()

            for idx, route in enumerate(jsonResponse['routes']):
                encoded = route['overview_polyline']['points']
                polyline = self._decodePolyline(encoded)
                feat.setGeometry(QgsGeometry.fromPolyline(polyline))

                #feat.setAttributeMap({0:QVariant('test')})
                pr.addFeatures([feat])

                # commit changes
                vl.commitChanges()

            # add layer to map
            QgsMapLayerRegistry.instance().addMapLayer(vl)

            with open(r'/home/tschuster/Desktop/sample.json', 'w') as myfile:
                json.dump(jsonResponse, myfile)

        except Exception as e:
            QgsMessageLog.logMessage('Failed to write polyline layer \n' + e.message, 'Directions', QgsMessageLog.CRITICAL)

    def _decodePolyline(self, encoded):
        try:
            """Decodes a polyline that was encoded using the Google Maps method.

            See http://code.google.com/apis/maps/documentation/polylinealgorithm.html

            This is a straightforward Python port of Mark McClure's JavaScript polyline decoder
            (http://facstaff.unca.edu/mcmcclur/GoogleMaps/EncodePolyline/decode.js)
            and Peter Chng's PHP polyline decode
            (http://unitstep.net/blog/2008/08/02/decoding-google-maps-encoded-polylines-using-php/)


            http://seewah.blogspot.de/2009/11/gpolyline-decoding-in-python.html
            """

            encoded_len = len(encoded)
            index = 0
            array = []
            lat = 0
            lng = 0

            while index < encoded_len:

                b = 0
                shift = 0
                result = 0

                while True:
                    b = ord(encoded[index]) - 63
                    index = index + 1
                    result |= (b & 0x1f) << shift
                    shift += 5
                    if b < 0x20:
                        break

                dlat = ~(result >> 1) if result & 1 else result >> 1
                lat += dlat

                shift = 0
                result = 0

                while True:
                    b = ord(encoded[index]) - 63
                    index = index + 1
                    result |= (b & 0x1f) << shift
                    shift += 5
                    if b < 0x20:
                        break

                dlng = ~(result >> 1) if result & 1 else result >> 1
                lng += dlng

                array.append(QgsPoint(lng * 1e-5, lat * 1e-5))

            return array
        except Exception as e:
            QgsMessageLog.logMessage('Failed to decode polyline \n' + e.message, 'Directions', QgsMessageLog.CRITICAL)