# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_direction.ui'
#
# Created: Tue Oct  7 21:49:01 2014
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Direction(object):
    def setupUi(self, Direction):
        Direction.setObjectName(_fromUtf8("Direction"))
        Direction.resize(300, 237)
        self.formLayout = QtGui.QFormLayout(Direction)
        self.formLayout.setFieldGrowthPolicy(QtGui.QFormLayout.AllNonFixedFieldsGrow)
        self.formLayout.setObjectName(_fromUtf8("formLayout"))
        self.label_origin = QtGui.QLabel(Direction)
        self.label_origin.setObjectName(_fromUtf8("label_origin"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.label_origin)
        self.lineEdit_origin = QtGui.QLineEdit(Direction)
        self.lineEdit_origin.setObjectName(_fromUtf8("lineEdit_origin"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEdit_origin)
        self.label_destination = QtGui.QLabel(Direction)
        self.label_destination.setObjectName(_fromUtf8("label_destination"))
        self.formLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.label_destination)
        self.lineEdit_destination = QtGui.QLineEdit(Direction)
        self.lineEdit_destination.setObjectName(_fromUtf8("lineEdit_destination"))
        self.formLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEdit_destination)
        self.checkBox_alternatives = QtGui.QCheckBox(Direction)
        self.checkBox_alternatives.setObjectName(_fromUtf8("checkBox_alternatives"))
        self.formLayout.setWidget(2, QtGui.QFormLayout.SpanningRole, self.checkBox_alternatives)
        self.groupBox_restrictions = QtGui.QGroupBox(Direction)
        self.groupBox_restrictions.setObjectName(_fromUtf8("groupBox_restrictions"))
        self.gridLayout = QtGui.QGridLayout(self.groupBox_restrictions)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.checkBox_avoidFerries = QtGui.QCheckBox(self.groupBox_restrictions)
        self.checkBox_avoidFerries.setObjectName(_fromUtf8("checkBox_avoidFerries"))
        self.gridLayout.addWidget(self.checkBox_avoidFerries, 2, 0, 1, 1)
        self.checkBox_avoidTolls = QtGui.QCheckBox(self.groupBox_restrictions)
        self.checkBox_avoidTolls.setObjectName(_fromUtf8("checkBox_avoidTolls"))
        self.gridLayout.addWidget(self.checkBox_avoidTolls, 0, 0, 1, 1)
        self.checkBox_avoidHighways = QtGui.QCheckBox(self.groupBox_restrictions)
        self.checkBox_avoidHighways.setObjectName(_fromUtf8("checkBox_avoidHighways"))
        self.gridLayout.addWidget(self.checkBox_avoidHighways, 0, 1, 1, 1)
        self.formLayout.setWidget(3, QtGui.QFormLayout.SpanningRole, self.groupBox_restrictions)
        self.buttonBox = QtGui.QDialogButtonBox(Direction)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.formLayout.setWidget(5, QtGui.QFormLayout.SpanningRole, self.buttonBox)

        self.retranslateUi(Direction)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), Direction.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), Direction.reject)
        QtCore.QMetaObject.connectSlotsByName(Direction)
        Direction.setTabOrder(self.lineEdit_origin, self.lineEdit_destination)
        Direction.setTabOrder(self.lineEdit_destination, self.checkBox_alternatives)
        Direction.setTabOrder(self.checkBox_alternatives, self.checkBox_avoidTolls)
        Direction.setTabOrder(self.checkBox_avoidTolls, self.checkBox_avoidHighways)
        Direction.setTabOrder(self.checkBox_avoidHighways, self.checkBox_avoidFerries)
        Direction.setTabOrder(self.checkBox_avoidFerries, self.buttonBox)

    def retranslateUi(self, Direction):
        Direction.setWindowTitle(_translate("Direction", "Direction", None))
        self.label_origin.setText(_translate("Direction", "Origin", None))
        self.label_destination.setText(_translate("Direction", "Destination", None))
        self.checkBox_alternatives.setText(_translate("Direction", "Alternatives", None))
        self.groupBox_restrictions.setTitle(_translate("Direction", "Routing Restrictions", None))
        self.checkBox_avoidFerries.setText(_translate("Direction", "Avoid ferries", None))
        self.checkBox_avoidTolls.setText(_translate("Direction", "Avoid tolls", None))
        self.checkBox_avoidHighways.setText(_translate("Direction", "Avoid highways", None))

